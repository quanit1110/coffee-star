package com.coffeestar.service.impl;

import com.coffeestar.constant.MessageConstant;
import com.coffeestar.entities.RoleDB;
import com.coffeestar.repositories.RoleDBRepository;
import com.coffeestar.common.ErrorUtils;
import com.coffeestar.common.ResponseStatus;
import com.coffeestar.common.SuccessUtils;
import com.coffeestar.payload.request.auth.RoleRequest;
import com.coffeestar.payload.response.RoleResponseDTO;
import com.coffeestar.service.base.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleDBRepository roleDBRepository;
    @Override
    public ResponseStatus saveRole(RoleRequest roleRequest) {
        RoleDB roleDB = roleDBRepository.findFirstByRoleName(roleRequest.getRoleName()).orElse(new RoleDB());
        if(Objects.nonNull(roleDB.getId())) return ErrorUtils.errorMessage(MessageConstant.ROLE_EXIST.getCode(), MessageConstant.ROLE_EXIST.getDescription());
        BeanUtils.copyProperties(roleRequest, roleDB);
        roleDB.setCreatedDate(new Date());
        roleDB.setUpdatedDate(new Date());
        roleDB.setRoleName(roleRequest.getRoleName().toUpperCase());
        roleDBRepository.saveAndFlush(roleDB);
        return SuccessUtils.successMessage(MessageConstant.SAVE_SUCCESS.getCode(), MessageConstant.SAVE_SUCCESS.getDescription());
    }

    @Override
    public List<RoleResponseDTO> getAllRole() {
        return roleDBRepository.findAll().stream().map(roleDB -> {
            RoleResponseDTO roleResponseDTO = new RoleResponseDTO();
            BeanUtils.copyProperties(roleDB, roleResponseDTO);
            return roleResponseDTO;
        }).collect(Collectors.toList());
    }

    @Override
    public Optional<RoleDB> getRoleByRoleName(String role) {
        return roleDBRepository.findFirstByRoleName(role);
    }
}
