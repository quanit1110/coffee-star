package com.coffeestar.service.impl;

import com.coffeestar.entities.RoleDB;
import com.coffeestar.entities.UserDB;
import com.coffeestar.repositories.RoleDBRepository;
import com.coffeestar.repositories.TokenRepository;
import com.coffeestar.repositories.UserDBRepository;
import com.coffeestar.common.TokenType;
import com.coffeestar.config.jwt.JwtUtils;
import com.coffeestar.entities.Token;
import com.coffeestar.payload.request.auth.UserRequest;
import com.coffeestar.payload.response.AuthenticationResponse;
import com.coffeestar.payload.response.UserResponseDTO;
import com.coffeestar.service.base.UserDBService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDBServiceImpl implements UserDBService {
    private final UserDBRepository userDBRepository;
    private final RoleDBRepository roleDBRepository;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    @Override
    public AuthenticationResponse saveUser(UserRequest userRequest) {
        UserDB userDB = new UserDB();
        BeanUtils.copyProperties(userRequest, userDB);
        userDB.setPassword(encoder.encode(userRequest.getPassword()));
        userDB.setCreatedDate(new Date());
        userDB.setUpdatedDate(new Date());
        UserDB user = userDBRepository.saveAndFlush(userDB);
        String token = jwtUtils.generateToken(user);
        saveUserToken(user, token);
        return AuthenticationResponse.builder()
                .accessToken(token)
                .refreshToken(jwtUtils.generateRefreshToken(user))
                .build();
    }

    @Override
    public List<UserResponseDTO> getAllUser() {
        return userDBRepository.findAll().stream().map(userDB -> {
            UserResponseDTO userResponseDTO = new UserResponseDTO();
            BeanUtils.copyProperties(userDB, userResponseDTO);
            return userResponseDTO;
        }).collect(Collectors.toList());
    }

    @Override
    public boolean isExistAccount(String username, String email) {
        Optional<UserDB> userDB = userDBRepository.findByUsernameOrEmail(username, email);
        return userDB.isPresent();
    }

    @Override
    public boolean isInValidRoles(Set<RoleDB> strRoles) {
        if(CollectionUtils.isEmpty(strRoles)) return true;
        for(RoleDB role : strRoles) {
            RoleDB roleDB = roleDBRepository.findFirstByRoleName(role.getRoleName()).orElse(null);
            if(Objects.isNull(roleDB)) return true;
        }
        return false;
    }

    private void saveUserToken(UserDB user, String jwtToken) {
        var token = Token.builder()
                .userdb(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }
}
