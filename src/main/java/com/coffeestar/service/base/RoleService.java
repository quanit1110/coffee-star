package com.coffeestar.service.base;

import com.coffeestar.entities.RoleDB;
import com.coffeestar.common.ResponseStatus;
import com.coffeestar.payload.request.auth.RoleRequest;
import com.coffeestar.payload.response.RoleResponseDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface RoleService {
    ResponseStatus saveRole(RoleRequest roleRequest);
    List<RoleResponseDTO> getAllRole();
    Optional<RoleDB> getRoleByRoleName(String role);
}
