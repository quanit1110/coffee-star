package com.coffeestar.service.base;

import com.coffeestar.entities.RoleDB;
import com.coffeestar.payload.request.auth.UserRequest;
import com.coffeestar.payload.response.AuthenticationResponse;
import com.coffeestar.payload.response.UserResponseDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface UserDBService {
    AuthenticationResponse saveUser(UserRequest userRequest);
    List<UserResponseDTO> getAllUser();
    boolean isExistAccount(String username, String email);
    boolean isInValidRoles(Set<RoleDB> strRoles);
}
