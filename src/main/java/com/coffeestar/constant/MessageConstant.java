package com.coffeestar.constant;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public enum MessageConstant {
    SAVE_SUCCESS("200", "Save data success"),
    SUCCESS("200", "Success"),
    USER_NOT_FOUND("USR_000001", "User not found in system"),
    USER_EXIST("USR_000002", "User is exist in system"),
    ROLE_EXIST("ROL_000002", "Role is exist in system"),
    ROLE_NOT_FOUND("ROL_000001", "Role is not exist in system");
    private final String code;
    private final String description;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
