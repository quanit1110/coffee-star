package com.coffeestar.repositories;

import com.coffeestar.entities.RoleDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleDBRepository extends JpaRepository<RoleDB, Integer> {
    Optional<RoleDB> findFirstByRoleName(String roleName);
}
