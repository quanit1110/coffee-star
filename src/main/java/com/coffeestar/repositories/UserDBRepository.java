package com.coffeestar.repositories;

import com.coffeestar.entities.UserDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDBRepository extends JpaRepository<UserDB, Integer> {
    Optional<UserDB> findByUsername(String username);
    Optional<UserDB> findByUsernameOrEmail(String username, String email);
}
