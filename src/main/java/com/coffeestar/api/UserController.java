package com.coffeestar.api;

import com.coffeestar.common.ResponseWithBody;
import com.coffeestar.common.SuccessUtils;
import com.coffeestar.constant.MessageConstant;
import com.coffeestar.payload.response.UserResponseDTO;
import com.coffeestar.service.base.UserDBService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserDBService userDBService;
    @GetMapping("/user/get-all")
    public ResponseWithBody<List<UserResponseDTO>> getAllUser(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String bearerToken ) {
        ResponseWithBody<List<UserResponseDTO>> responseWithBody = new ResponseWithBody<>();
        responseWithBody.setBody(userDBService.getAllUser());
        responseWithBody.setMessages(SuccessUtils.successMessage(MessageConstant.SUCCESS.getCode(), MessageConstant.SUCCESS.getDescription()));
        return responseWithBody;
    }

    /*@PostMapping(path = "/user/add-new", consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseWithBody<UserRequest> getAllUser(@RequestHeader(value = HttpHeaders.AUTHORIZATION) String bearerToken,
                                                    @RequestBody UserRequest userRequest) {
        ResponseWithBody<UserRequest> responseWithBody = new ResponseWithBody<>();
        responseWithBody.setBody(userRequest);
        responseWithBody.setMessages(userDBService.saveUser(userRequest));
        return responseWithBody;
    }*/
}
