package com.coffeestar.api;

import com.coffeestar.common.ErrorUtils;
import com.coffeestar.common.ResponseWithBody;
import com.coffeestar.config.jwt.JwtUtils;
import com.coffeestar.config.services.UserDetailsImpl;
import com.coffeestar.constant.MessageConstant;
import com.coffeestar.payload.request.auth.LoginRequest;
import com.coffeestar.payload.request.auth.UserRequest;
import com.coffeestar.payload.response.AuthenticationResponse;
import com.coffeestar.service.base.UserDBService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class LoginController {

    private final JwtUtils jwtUtils;
    private final AuthenticationManager authenticationManager;
    private final UserDBService userDBService;

    @PostMapping("/sign-in")
    public ResponseEntity<ResponseWithBody<AuthenticationResponse>> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        ResponseWithBody<AuthenticationResponse> responseWithBody = new ResponseWithBody<>();
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        String jwtCookie = jwtUtils.generateToken(userDetails);
        responseWithBody.setBody(AuthenticationResponse.builder().accessToken(jwtCookie)
                .refreshToken(jwtUtils.generateRefreshToken(userDetails)).build());
        return ResponseEntity.ok()
                .body(responseWithBody);
    }

    @PostMapping("/sign-up")
    public ResponseEntity<ResponseWithBody<AuthenticationResponse>> registerUser(@Valid @RequestBody UserRequest signUpRequest) {
        ResponseWithBody<AuthenticationResponse> userRequestResponseWithBody = new ResponseWithBody<>();
        if (userDBService.isExistAccount(signUpRequest.getUsername(), signUpRequest.getEmail())) {
            userRequestResponseWithBody.setBody(null);
            userRequestResponseWithBody.setMessages(ErrorUtils.errorMessage(MessageConstant.USER_EXIST.getCode(), MessageConstant.USER_EXIST.getDescription()));
            return ResponseEntity.badRequest().body(userRequestResponseWithBody);
        }
        if(userDBService.isInValidRoles(signUpRequest.getRoles())) {
            userRequestResponseWithBody.setBody(null);
            userRequestResponseWithBody.setMessages(ErrorUtils.errorMessage(MessageConstant.ROLE_NOT_FOUND.getCode(), MessageConstant.ROLE_NOT_FOUND.getDescription()));
            return ResponseEntity.badRequest().body(userRequestResponseWithBody);
        }
        userRequestResponseWithBody.setBody(userDBService.saveUser(signUpRequest));
        return ResponseEntity.ok(userRequestResponseWithBody);
    }

    @PostMapping("/sign-out")
    public ResponseEntity<?> logoutUser(HttpServletRequest request,
                                        HttpServletResponse response) {
//        ResponseCookie cookie = jwtUtils.generateRefreshToken();
        return ResponseEntity.ok()
                .body("You've been signed out!");
    }
}
