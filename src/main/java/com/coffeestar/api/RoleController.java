package com.coffeestar.api;

import com.coffeestar.constant.MessageConstant;
import com.coffeestar.payload.request.auth.RoleRequest;
import com.coffeestar.payload.response.RoleResponseDTO;
import com.coffeestar.service.base.RoleService;
import com.coffeestar.common.ResponseWithBody;
import com.coffeestar.common.SuccessUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RoleController {
    private final RoleService roleService;
    @GetMapping("/role/get-all")
    public ResponseWithBody<List<RoleResponseDTO>> getAllUser() {
        ResponseWithBody<List<RoleResponseDTO>> responseWithBody = new ResponseWithBody<>();
        responseWithBody.setBody(roleService.getAllRole());
        responseWithBody.setMessages(SuccessUtils.successMessage(MessageConstant.SUCCESS.getCode(), MessageConstant.SUCCESS.getDescription()));
        return responseWithBody;
    }

    @PostMapping(path = "/role/add-new", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseWithBody<RoleRequest> getAllUser(@RequestBody RoleRequest roleRequest) {
        ResponseWithBody<RoleRequest> responseWithBody = new ResponseWithBody<>();
        responseWithBody.setBody(roleRequest);
        responseWithBody.setMessages(roleService.saveRole(roleRequest));
        return responseWithBody;
    }
}
