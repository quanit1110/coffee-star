package com.coffeestar.payload.request.auth;

import com.coffeestar.dto.RequestCommon;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class RoleRequest extends RequestCommon {
    private String roleName;
    private String roleDescription;
}
