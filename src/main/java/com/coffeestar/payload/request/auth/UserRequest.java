package com.coffeestar.payload.request.auth;

import com.coffeestar.entities.RoleDB;
import com.coffeestar.dto.RequestCommon;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
public class UserRequest extends RequestCommon {
    private String username;
    private String password;
    private String email;
    private Set<RoleDB> roles;
    private Boolean isAccountNonExpired;
    private Boolean isAccountNonLocked;
    private Boolean isCredentialsNonExpired;
    private Boolean isEnabled;
}
