package com.coffeestar;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info = @Info(title = "${openapi.info.title}", version = "${openapi.info.version}",
		description = "${openapi.info.description}", contact = @Contact(name = "${openapi.info.contact.name}",
		email = "${openapi.info.contact.email}"), license = @License(url = "${openapi.info.license.url}",
		name = "${openapi.info.license.name}")))
@SpringBootApplication
public class CoffeeStarApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoffeeStarApplication.class, args);
	}

}
