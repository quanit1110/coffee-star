package com.coffeestar.common;

public class ErrorUtils {
    public static ResponseStatus errorMessage(String code, String message) {
        return ResponseStatus.builder().code(code).message(message).build();
    }
}
