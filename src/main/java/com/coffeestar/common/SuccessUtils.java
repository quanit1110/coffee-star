package com.coffeestar.common;

public class SuccessUtils {
    public static ResponseStatus successMessage(String code, String message) {
        return ResponseStatus.builder().code(code).message(message).build();
    }
}
